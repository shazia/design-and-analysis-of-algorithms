
def fib_iterative(n):
    x = 0
    y = 1
    for i in range(2, n):
        z = x + y
        x = y
        y = z
    return x


def fib_recursive(n):
    if n == 0 or n == 1:
        return 1
    else:
        return fib_recursive(n - 1) + fib_recursive(n - 2)


if __name__ == "__main__":
    print([fib_iterative(x) for x in range(11)])
    print([fib_recursive(x) for x in range(11)])
