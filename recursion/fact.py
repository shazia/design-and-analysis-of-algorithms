
def fact_iterative(n):
    result = 1
    while n > 1:
        result = result * n
        n = n - 1
    return result


def fact_recursive(n):
    if n <= 1:
        return 1
    else:
        return n * fact_recursive(n - 1)


if __name__ == "__main__":
    print(fact_iterative(10))
    print(fact_recursive(10))
