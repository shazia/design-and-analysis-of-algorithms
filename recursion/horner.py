
def horners_rule_recursive(x, coefs):
    def iter(init, x, coefs, k):
        if k < len(coefs):
            return iter(init * x + coefs[k], x, coefs, k + 1)
        else:
            return init

    return iter(0, x, coefs, 0)


def horners_rule_iterative(x, coefs):
    result = 0
    for c in coefs:
        result = result * x + c
    return result


if __name__ == "__main__":
    result = horners_rule_recursive(2, [3, 2, 1])
    print(result)
