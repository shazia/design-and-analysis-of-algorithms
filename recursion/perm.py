
def swap(elements, x, y):
    t = elements[x]
    elements[x] = elements[y]
    elements[y] = t


def perms(elements, k):
    if len(elements) == k:
        print(elements)
    else:
        for i in range(k, len(elements)):
            swap(elements, i, k)
            perms(elements, k + 1)
            swap(elements, k, i)


if __name__ == "__main__":
    perms(['A', 'B', 'C'], 0)
