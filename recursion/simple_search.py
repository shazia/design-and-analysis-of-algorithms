
def search(elements, x):
    for i in range(len(elements)):
        if elements[i] == x:
            return i
    return -1


if __name__ == "__main__":
    print(search([6, 7, 2, 3, 5, 4], 5))
    print(search([5, 6, 7], 0))
