def towers_of_hanoi(source, dest, pivot, no_of_disks):
    if no_of_disks >= 1:
        towers_of_hanoi(source, pivot, dest, no_of_disks - 1)
        print("Move disk {} from {} to {}".format(no_of_disks, source, dest))
        towers_of_hanoi(pivot, dest, source, no_of_disks - 1)


if __name__ == "__main__":
    towers_of_hanoi('A', 'B', 'C', 3)
